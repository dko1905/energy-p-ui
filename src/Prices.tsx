import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  Title,
  Tooltip,
  BarOptions,
} from "chart.js";
import { ChangeEvent, useEffect, useState } from "react";
import { Bar } from "react-chartjs-2";
import DatePicker from "react-date-picker";
import { useQuery } from "react-query";

interface EnergyPrice {
  ID: string;
  TS: string; // Date
  FetchTS: string; // Date
  Value: number;
}

interface FetchOptions {
  start: Date;
  end: Date;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

function Prices() {
  const [fetchOptions, setFetchOptions] = useState<FetchOptions>({
    start: new Date(),
    end: new Date(Date.now() + 60 * 60 * 2 * 1000),
  })

  const query = useQuery(
    "prices",
    () => {
      let url = "https://pp.0chaos.eu";
      url += "/api/price";
      url += "?start=" + fetchOptions.start.toISOString();
      url += "&end=" + fetchOptions.end.toISOString();
      return fetch(url).then(async (body) => {
        const data: EnergyPrice[] = await body.json();
        return data.map(item => ({...item, value: new Number(item.Value)}))
      });
    },
    { enabled: false }
  );

  useEffect(() => {
    query.refetch();
  }, [fetchOptions]);

  return (
    <div>
      <OptionsPanel onChange={setFetchOptions} />
      {query.status == "loading" && <b>Loading...</b>}
      {query.status == "error" && <b>Error: {String(query.error)}</b>}
      {query.status == "success" && (
        <div>
          <BarGraph data={query.data} />
        </div>
      )}
    </div>
  );
}

function OptionsPanel(props: { onChange: (options: FetchOptions) => void }) {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const onSubmit = (e: any) => {
    e.preventDefault();
    props.onChange({
      start: startDate,
      end: endDate,
    });
  };
  const onStartDateChange = (e: any) => {
    setStartDate(new Date(e.valueAsNumber));
  };
  const onEndDateChange = (e: any) => {
    setEndDate(new Date(e.valueAsNumber));
  };

  return (
    <form onSubmit={onSubmit}>
      <label>
        Start date: <br />
        <DatePicker onChange={setStartDate} value={startDate} />
      </label>
      <br />
      <label>
        End date: <br />
        <DatePicker onChange={setEndDate} value={endDate} />
      </label>
      <br />
      <button type="submit">Apply</button>
    </form>
  );
}

function BarGraph(props: { data: EnergyPrice[] }) {
  const formatData = (data: EnergyPrice[]) => {
    const highestPrice = Math.max(...data.map((v) => v.Value));
    const priceColorMultiplier = 360 / highestPrice;

    return {
      scales: {
        x: {
          grid: {
            offset: false,
          },
        },
      },
      labels: props.data.map((value) =>
        new Date(value.TS).toLocaleString(undefined, {
          day: "numeric",
          month: "short",
          hour: "2-digit",
        })
      ),
      datasets: [
        {
          label: "Price A",
          data: props.data.map((value) => value.Value),
          backgroundColor: props.data.map((value) => {
            let color = "rgb(";
            color += `${(value.Value) * priceColorMultiplier}`;
            color += `, 0`;
            color += `, ${(highestPrice - (value.Value)) * priceColorMultiplier}`;
            color += ", 0.5)";
            return color;
          }),
        },
      ],
    };
  };

  const [graphData, setGraphData] = useState(formatData(props.data));

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top" as const,
      },
      title: {
        display: true,
        text: "Chart.js Bar Chart",
      },
    },
  };

  useEffect(() => {
    setGraphData(formatData(props.data));
  }, [props.data]);

  return <div>{<Bar options={options} data={graphData} />}</div>;
}

export default Prices;
