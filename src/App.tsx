import { QueryClient, QueryClientProvider, useQuery } from "react-query";
import Prices from "./Prices";

function App() {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <div id="App">
        <main>
          <Prices />
        </main>
        <footer>
          <div>
            <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">AGPL-3.0</a>
          </div>
        </footer>
      </div>
    </QueryClientProvider>
  );
}

export default App;
